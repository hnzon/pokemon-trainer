import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginForm } from './components/forms/loginForm/login-form.component';
import { PageNotFound } from './pages/page-not-found/page-not-found.page';
import { AppContainerComponent } from './components/container/container.component';
import { LoginContainer } from './pages/login/login.container';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PokemonCataloguePage } from './pages/pokemon-catalogue-page/pokemon-catalogue.page';
import { TrainerPage } from './pages/trainer-page/trainer.page';
import { PokemonComponent } from './components/pokemon/pokemon.component';
import { PokemonDetailPage } from './pages/pokemon-detail-page/pokemon-detail.page';

@NgModule({
  declarations: [
    AppComponent,
    LoginForm,
    LoginContainer,
    PokemonCataloguePage,
    TrainerPage,
    NavbarComponent,
    PageNotFound,
    AppContainerComponent,
    PokemonComponent,
    PokemonDetailPage,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

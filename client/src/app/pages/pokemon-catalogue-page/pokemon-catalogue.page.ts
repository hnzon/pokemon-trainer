import { Component } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-pokemon-catalogue-page',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.scss'],
})
export class PokemonCataloguePage {
  constructor(
    private readonly pokemonService: PokemonService,
    public router: Router
  ) {}

  get pokemons(): Pokemon[] {
    return this.pokemonService.pokemons;
  }

  ngOnInit(): void {
    this.pokemonService.fetchPokemon();
  }

  onNextClick(): void {
    console.log('next clicked');
    this.pokemonService.next();
  }

  onPrevClick(): void {
    console.log('prev clicked');
    this.pokemonService.prev();
  }

  navigateToDetailPage(selectedPokemon: Pokemon) {
    let navigationExtras: NavigationExtras = {
      state: {
        pokemon: selectedPokemon,
      },
    };
    this.router.navigate([`pokemon/${selectedPokemon.name}`], navigationExtras);
  }
}

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppRoutes } from 'src/enums/app-routes.enum';

@Component({
  selector: 'app-login-container',
  templateUrl: 'login.container.html',
  styles: [
    `
      label,
      input {
        display: block;
        width: 100%;
        margin-bottom: 1em;
      }
    `,
  ],
})
export class LoginContainer {
  constructor(private readonly router: Router) {}

  handleSuccessfullLogin(): void {
    this.router.navigateByUrl(AppRoutes.TRAINER);
  }
}

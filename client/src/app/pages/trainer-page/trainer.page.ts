import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.scss'],
})
export class TrainerPage {
  error: string = '';

  constructor(
    private readonly trainerService: TrainerService,
    public router: Router
  ) {}

  get pokemons(): Pokemon[] {
    return this.trainerService.pokemons;
  }

  get trainerName() {
    return this.trainerService.trainerName;
  }

  ngOnInit() {
    this.trainerService.getTrainer();
  }

  navigateToDetailPage(selectedPokemon: Pokemon) {
    let navigationExtras: NavigationExtras = {
      state: {
        pokemon: selectedPokemon,
      },
    };
    this.router.navigate([`pokemon/${selectedPokemon.name}`], navigationExtras);
  }
}

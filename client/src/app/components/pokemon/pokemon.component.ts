import { Component, Input } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss'],
})
export class PokemonComponent {
  @Input() pokemon: Pokemon;

  assignBackgroundColor(type: string): string {
    let color = 'transparent';
    switch (type) {
      case 'electric':
        color = '#bd8d0b';
        break;
      case 'fire':
        color = '#8a2c00';
        break;
      case 'water':
        color = '#00165e';
        break;
      case 'grass':
        color = '#285917';
        break;
      case 'bug':
        color = '#465917';
        break;
      case 'rock':
        color = '#636363';
        break;
      case 'fighting':
        color = '#800800';
        break;
      case 'ghost':
        color = '#39003d';
        break;
      case 'psychic':
        color = '#a555ab';
        break;
      case 'poison':
        color = '#57125c';
        break;
      case 'ice':
        color = '#376b6b';
        break;
      case 'ground':
        color = '#452f09';
        break;
      case 'dark':
        color = '#242424';
        break;
      case 'normal':
        color = '#5e5e4a';
        break;
      case 'fairy':
        color = '#a12045';
        break;
      case 'dragon':
        color = '#8c6320';
        break;
      case 'steel':
        color = '#8a8a8a';
        break;
      default:
        color = 'transparent';
    }
    return color;
  }
}

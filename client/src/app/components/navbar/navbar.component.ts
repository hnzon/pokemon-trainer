import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { LoginService } from 'src/app/services/login.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  public isLoggedIn: boolean = false;
  private CurrentRoute = '/';

  constructor(
    private readonly sessionService: SessionService,
    public loginService: LoginService,
    private router: Router
  ) {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map((event) => event as NavigationEnd)
      )
      .subscribe(
        (evt) => (this.CurrentRoute = evt.url),
        (error) => {
          console.log('error: ' + error);
        }
      );
  }

  isCurrentRoute(route: string): boolean {
    console.log(route, this.CurrentRoute);
    return this.CurrentRoute === route;
  }

  get hasActiveSession(): boolean {
    return this.sessionService.isLoggedIn();
  }
}

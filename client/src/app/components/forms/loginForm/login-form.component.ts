import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { getStorage } from 'src/app/utils/storage.utils';
import { LoginService } from '../../../services/login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginForm implements OnInit {
  @Output() success: EventEmitter<void> = new EventEmitter();

  public registerTrainerForm: FormGroup = new FormGroup({
    trainerName: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
    ]),
  });

  constructor(private readonly loginService: LoginService) {}

  ngOnInit(): void {
    const existingTrainer = getStorage<any>('pokemon-trainer');
    if (existingTrainer !== null) {
      this.success.emit();
    }
  }

  public get trainerName(): AbstractControl {
    return this.registerTrainerForm.get('trainerName');
  }

  get loading(): boolean {
    return this.loginService.loading;
  }

  get error(): string {
    return this.loginService.error;
  }

  onLoginClick() {
    console.log(this.registerTrainerForm.value);
    const { trainerName } = this.registerTrainerForm.value;
    this.loginService
      .login(trainerName)
      .subscribe(
        this.handleSuccessfullLogin.bind(this),
        this.handleLoginError.bind(this)
      );
  }

  onRegisterClick() {
    const { trainerName } = this.registerTrainerForm.value;
    this.loginService.register(trainerName).subscribe((response) => {
      this.success.emit();
    });
  }

  handleSuccessfullLogin(trainer): void {
    this.success.emit();
  }

  handleLoginError(error): void {
    if (this.loginService.errorCount >= 3) {
    }
    console.log(error);
  }
}

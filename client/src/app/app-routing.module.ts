import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginContainer } from './pages/login/login.container';
import { SessionGuard } from './guards/session.guard';
import { PageNotFound } from './pages/page-not-found/page-not-found.page';
import { PokemonCataloguePage } from './pages/pokemon-catalogue-page/pokemon-catalogue.page';
import { TrainerPage } from './pages/trainer-page/trainer.page';
import { PokemonDetailPage } from './pages/pokemon-detail-page/pokemon-detail.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login',
  },
  {
    path: 'login',
    component: LoginContainer,
  },
  {
    path: 'trainer',
    component: TrainerPage,
    canActivate: [SessionGuard],
  },

  {
    path: 'pokemon-catalogue',
    component: PokemonCataloguePage,
    canActivate: [SessionGuard],
  },
  {
    path: 'pokemon/:name',
    component: PokemonDetailPage,
    canActivate: [SessionGuard],
  },

  {
    path: '**',
    component: PageNotFound,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

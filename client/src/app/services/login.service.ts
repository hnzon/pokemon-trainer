import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { finalize, map, mergeMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { logoutUser, setStorage } from '../utils/storage.utils';
import { Router } from '@angular/router';
import { SessionService } from './session.service';
import { AppRoutes } from 'src/enums/app-routes.enum';
import { PokemonService } from './pokemon.service';
const { trainerAPI } = environment;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  public loading: boolean = false;
  public error: string = '';
  public errorCount: number = 0;

  constructor(
    private readonly http: HttpClient,
    private sessionService: SessionService,
    private pokemonService: PokemonService,
    private router: Router
  ) {}

  private setTrainer(trainer: any) {
    setStorage('pokemon-trainer', trainer);
  }

  register(name: string): Observable<any> {
    return this.http
      .post(`${trainerAPI}/trainers`, {
        name,
        pokemon: [],
      })
      .pipe(
        map((trainer) => {
          this.setTrainer(trainer);
          return trainer;
        })
      );
  }

  login(trainerName: string): Observable<any> {
    this.loading = true;
    const login$ = this.http.get(`${trainerAPI}/trainers?name=${trainerName}`);
    const register$ = this.http.post(`${trainerAPI}/trainer`, {
      name: trainerName,
      pokemon: [],
    });

    //merge login and register observables together
    return login$.pipe(
      mergeMap((loginResponse: any[]) => {
        const trainer = loginResponse.pop();

        delete trainer.id;

        if (!trainer) {
          return register$;
        } else {
          return of(trainer);
        }
      }),
      tap((trainer) => {
        this.setTrainer(trainer);
      }),
      finalize(() => {
        this.loading = false;
      })
    );
  }

  logout() {
    console.log(this.sessionService.isLoggedIn());
    this.pokemonService.offset = 0;
    logoutUser();
    this.router.navigateByUrl(AppRoutes.LOGIN);
  }

  get id(): string {
    return this.isLoggedIn ? this.sessionService.trainerId : '';
  }

  get isLoggedIn(): boolean {
    return this.sessionService.trainerId !== '';
  }
}

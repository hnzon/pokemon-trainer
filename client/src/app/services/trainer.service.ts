import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { environment } from 'src/environments/environment';
const { trainerAPI } = environment;
import { getStorage } from '../utils/storage.utils';

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  public pokemons: Pokemon[] = [];
  public error: string = '';
  private trainer: Trainer;

  constructor(private readonly http: HttpClient) {}

  public showTrainerName() {
    this.getTrainer();
  }

  get trainerName(): string {
    return getStorage('pokemon-trainer').name;
  }

  public getTrainer() {
    const trainerName = this.trainerName;
    this.http.get(`${trainerAPI}/trainers?name=${trainerName}`).subscribe(
      (data: any) => {
        console.log('GET Request is successful ');
        this.trainer = data[0];
        this.pokemons = data[0].pokemon;
      },
      (error) => {
        console.log('Error', error);
      }
    );
  }

  public putPokemonToTrainer(pokemon) {
    console.log('adding ' + pokemon.name);

    this.pokemons.push(pokemon);
    this.http
      .patch(`${trainerAPI}/trainers/${this.trainer.id}`, {
        pokemon: this.pokemons,
      })
      .subscribe(
        (response) => {
          console.log('PATCH Request is successful ', response);
        },
        (error) => {
          console.log('Error', error);
        }
      );
  }
}

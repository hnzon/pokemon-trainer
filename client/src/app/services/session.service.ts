import { Injectable } from '@angular/core';
import { getStorage } from '../utils/storage.utils';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  isLoggedIn(): boolean {
    const trainer = getStorage('pokemon-trainer');
    return Boolean(trainer);
  }

  get trainerId(): string {
    return sessionStorage.getItem('pokemon-trainer') || '';
  }
  set trainerId(trainer: string) {
    sessionStorage.setItem('pokemon-trainer', trainer);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Pokemon } from '../models/pokemon.model';
import { throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { PokemonResponse } from '../models/pokemon-response.model';
@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  public pokemons: Pokemon[] = [];
  public error: string = '';
  private _offset = 0;

  constructor(private readonly http: HttpClient) {}

  public get offset() {
    return this._offset;
  }
  public set offset(value) {
    this._offset = value;
  }

  public next() {
    if (this.offset >= 1100) {
      console.log('end of results');
    } else {
      this.offset += 20;
      this.fetchPokemon();
      console.log(this.offset);
    }
  }

  public prev() {
    if (this.offset === 0) {
      return;
    }
    this.offset -= 20;
    this.fetchPokemon();
    console.log(this.offset);
  }

  fetchPokemon(): void {
    this.http
      .get<PokemonResponse>(
        `${environment.basePokemonAPIUrl}${environment.pokeLimit}&offset=${this.offset}`
      )
      .pipe(
        map((response: PokemonResponse) => {
          return response.results.map((pokemon: Pokemon) => ({
            ...pokemon,
            ...this.getIdAndImage(pokemon.url),
          }));
        })
      )
      .subscribe(
        (pokemons: Pokemon[]) => {
          pokemons.forEach((pokemon) => {
            this.getPokemonDetails(pokemon.url).subscribe((response: any) => {
              pokemon.weight = response.weight;
              pokemon.height = response.height;
              pokemon.stats = response.stats;
              pokemon.abilities = response.abilities;
              pokemon.types = response.types;
              pokemon.base_experience = response.base_experience;
              pokemon.moves = this.getSimpleMoves(response.moves.slice(0, 10));
            });
          });

          this.pokemons = pokemons;
        },
        (errorResponse: HttpErrorResponse) => {
          this.error = errorResponse.message;
        }
      );
  }

  private getSimpleMoves(moves) {
    let simpleMoves = [];
    moves.forEach((move) => {
      simpleMoves.push(move.move.name);
    });
    return simpleMoves;
  }

  private getPokemonDetails(url: string) {
    return this.http.get(url);
  }

  private getIdAndImage(url: string): any {
    const id = url.split('/').filter(Boolean).pop();
    return {
      id: Number(id),
      image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`,
    };
  }
}

import { PokemonAbilities } from '.././models/pokemon-abilities.model';
import { PokemonMoves } from '.././models/pokemon-moves-model';
import { PokemonStats } from '.././models/pokemon-stats.model';
import { PokemonTypes } from '.././models/pokemon-types.model';

export interface Pokemon {
  url?: string;
  types: PokemonTypes[];
  stats: PokemonStats[];
  name: string;
  height?: number;
  weight: number;
  abilities?: PokemonAbilities[];
  base_experience: number;
  moves?: PokemonMoves[];
  id: number;
  image?: string;
}

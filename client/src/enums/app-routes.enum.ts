export enum AppRoutes {
  LOGIN = '/login',
  POKEMON = '/pokemon-catalogue',
  POKEMON_DETAIL = '/pokemon/:name',
  TRAINER = '/trainer',
  HOME = '/',
}

export const environment = {
  production: false,
  basePokemonAPIUrl: 'https://pokeapi.co/api/v2/pokemon/',
  pokeLimit: '?limit=20',
  trainerAPI: 'http://localhost:3000',
};
